import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.util.HashSet;
import java.util.Iterator;

import javax.swing.*;
import javax.swing.border.TitledBorder;

public class UniqueStringsFrame extends JFrame {

    private static File fileIn = null;
    private static File fileOut = null;
    private static HashSet<String> file_read_content = null;
    private static HashSet<String> file_write_content = null;
    private static JProgressBar progress = null;
    private static JButton button_execute = null;
    private static Calculate calculate = null;

    public UniqueStringsFrame() {
        super("UniqueStrings");
        try {
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.print(e.getMessage());
        }
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        Image img = new ImageIcon(getClass().getResource("icon.png")).getImage();
        this.setIconImage(img);

        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        panel.add(Box.createVerticalGlue());

        JPanel first_file_panel = new JPanel();
        first_file_panel.setLayout(new BoxLayout(first_file_panel, BoxLayout.LINE_AXIS));
        first_file_panel.setBorder(new TitledBorder("Файл образец"));

        JPanel second_file_panel = new JPanel();
        second_file_panel.setLayout(new BoxLayout(second_file_panel, BoxLayout.LINE_AXIS));
        second_file_panel.setBorder(new TitledBorder("Файл с дубликатами"));

        JPanel execute_panel = new JPanel();
        execute_panel.setLayout(new BoxLayout(execute_panel, BoxLayout.LINE_AXIS));
        execute_panel.setBorder(new TitledBorder("Прогресс выполнения"));

        final JTextField label_fread = new JTextField("Файл не выбран");
        label_fread.setEnabled(false);

        final JTextField label_fwrite = new JTextField("файл не выбран");
        label_fwrite.setEnabled(false);

        panel.add(Box.createRigidArea(new Dimension(10, 10)));

        JButton button_read = new JButton("Обзор...");
        button_read.setAlignmentX(CENTER_ALIGNMENT);
        button_read.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    if (file.exists()) {
                        label_fread.setText(file.getPath());
                        fileIn = file;
                    } else {
                        label_fread.setText("файл не выбран");
                        fileIn = null;
                    }
                }
            }
        });



        JButton button_write = new JButton("Обзор...");
        button_write.setAlignmentX(CENTER_ALIGNMENT);
        button_write.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileopen = new JFileChooser();
                int ret = fileopen.showDialog(null, "Открыть файл");
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = fileopen.getSelectedFile();
                    if (file.exists()) {
                        label_fwrite.setText(file.getPath());
                        fileOut = file;
                    } else {
                        label_fwrite.setText("файл не выбран");
                        fileOut = null;
                    }
                }
            }
        });


        progress = new JProgressBar(0, 100);
        progress.setSize(200, 30);
        progress.setStringPainted(true);
        progress.setValue(0);
        progress.setString("не запущено");



        button_execute = new JButton("старт");
        button_execute.setSize(160, 30);
        button_execute.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (fileIn != null && fileOut != null) {
                    if (calculate != null && calculate.isAlive()) {
                        calculate.interrupt();
                        button_execute.setText("старт");
                    } else {
                        calculate = new Calculate();
                        button_execute.setText("Отмена");
                        calculate.start();
                    }
                }
            }
        });




        first_file_panel.add(label_fread);
        first_file_panel.add(button_read);
        panel.add(first_file_panel);

        second_file_panel.add(label_fwrite);
        second_file_panel.add(button_write);
        panel.add(second_file_panel);

        execute_panel.add(button_execute);
        execute_panel.add(progress);
        panel.add(execute_panel);
        panel.add(Box.createVerticalGlue());
        getContentPane().add(panel);
        pack();
        setSize(400, 200);
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public static HashSet<String> read(File file) {
        HashSet<String> array = new HashSet<String>();
        try
        {
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);

            String str;
            while ((str = br.readLine()) != null) {
                array.add(str);
            }
        } catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
        return array;
    }

    public static void write(File file, HashSet<String> array) {
        try(FileWriter writer = new FileWriter(file))
        {
            if (array.isEmpty()) {
                writer.write("");
            } else {
                for (String str : array) {
                    writer.append(str);
                    writer.append("\r\n");
                }
            }
            writer.flush();
        }
        catch(IOException ex){
            System.out.println(ex.getMessage());
        }
    }

    public static void main(String[] args) {
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                JFrame.setDefaultLookAndFeelDecorated(false);
                JDialog.setDefaultLookAndFeelDecorated(false);
                new UniqueStringsFrame();
            }
        });
    }

    class Calculate extends Thread
    {
        @Override
        public void run()
        {
            progress.setValue(0);
            progress.setString("Чтение первого файла");
            file_read_content = read(fileIn);
            progress.setString("Читение второго файла");
            file_write_content = read(fileOut);
            HashSet<String> array = new HashSet<String>();
            int size = file_write_content.size();
            progress.setString(null);
            progress.setMaximum(size);
            String str;
            int i=0;
            ProgressUpdater progress_updater = new ProgressUpdater();
            progress_updater.start();
            Iterator<String> iterator = file_write_content.iterator();
            for (;iterator.hasNext();) {
                str = iterator.next();
                if (!file_read_content.contains(str)) {
                    array.add(str);
                }
                progress_updater.setNewValue(i);
                i++;
                if (Thread.interrupted()) {
                    progress_updater.interrupt();
                    progress.setString("Отменено");
                    return;
                }
            }
            progress_updater.stopCorrect();
            progress.setString("запись в файл");
            write(fileOut, array);
            progress.setString("Готово");
            button_execute.setText("старт");
        }
    }

    class ProgressUpdater extends Thread {
        private int new_value;
        private boolean is_done = false;
        @Override
        public void run() {
            while (true) {
                progress.setValue(this.new_value);
                if (Thread.interrupted()) {
                    if (is_done) {
                        progress.setValue(progress.getMaximum());
                    } else {
                        progress.setValue(0);
                    }
                    return;
                }
                try {
                    sleep(100);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                }
            }
        }

        public void setNewValue(int new_value) {
            this.new_value = new_value;
        }

        public void stopCorrect() {
            is_done = true;
            interrupt();
        }
    }
}